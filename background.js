// chrome.app.runtime.onLaunched.addListener(function() {
//   // chrome.tabs.create({'url': chrome.extension.getURL('index.html')}, function(tab){

//   // });
//   chrome.tabs.create({ 'url': 'chrome://extensions/?options=' + chrome.runtime.id });
// });

var port = null;
var HOUR_MS = 1000 * 60 * 60;
var timers = [];
var msg = null;


function ringAlarm(alarmHours,alarmMinutes,desc){
	alert('Appointment: "'+desc+'" is in 10 min');
}

function setTimer(alarmHours, alarmMinutes, desc) {
  var alarmTime = (alarmHours * 60 + alarmMinutes) * 60 * 1000;
  var d = new Date();
  var now = d.getHours() * HOUR_MS +
            d.getMinutes() * 60 * 1000 +
            d.getSeconds() * 1000;
  var delta = (alarmTime - now);

  if (delta >= -5000 && delta < 1000) {
    ringAlarm(alarmHours, alarmMinutes,desc);
    if (port) {
      port.postMessage({'cmd': 'anim'});
    }
    return null;
  }

  if (delta < 0) {
    delta += HOUR_MS * 24;
  }
  if (delta >= 1000) {
    if (delta > HOUR_MS) {
      delta = HOUR_MS;
    }
    console.log('Timer set for ' + delta + ' ms');
    return window.setTimeout(resetTimers, delta);
  }

  return null;
};

var resetTimers = function(){
	
	if (timers.length) {
    	timers.forEach(function(t){window.clearTimeout(t)});
    	timers=[];
  	}
  	var d_tt = localStorage['apmtData']?JSON.parse(localStorage['apmtData']):[],i=0;
  	//for(i=1;localStorage['h'+i];i++)
  	if(d_tt)
  	d_tt.forEach(function(v){
      var min=parseInt(v.min),hr = parseInt(v.hr);
      if(min<10) {min = 60-min; hr--;}
      else min-=10;
      timers.push(setTimer(hr, min,v.desc));});
 // setTimer(3,51);
		
}

function onLocalStorageChange() {
  resetTimers();
}

function initBackground() {
	localStorage['apmtLogin'] = 'Rahul';
  window.addEventListener('storage', onLocalStorageChange, false);

  chrome.runtime.onConnect.addListener(function(popupPort) {
    port = popupPort;
    port.onDisconnect.addListener(function() {
      port = null;
    });
  });
}
chrome.browserAction.onClicked.addListener(function() {
	chrome.tabs.create({url: chrome.extension.getURL('index.html')}, function(tab) {

	})
});
localStorage.clear();
initBackground();
resetTimers();
