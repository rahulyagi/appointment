$('.md-trigger').click(function(){
      $('#modal-16').addClass('md-show');
      //$('html').addClass('md-perspective');
    })
$('.md-close').click(function(){
	$('#modal-16').removeClass('md-show');
})
$(document.body).on('click','.appointment',function(){
	var data = JSON.parse($(this).attr('data'))
	$('.txtdesc').val(data.desc);
	$('.lstHr').val(data.hr);
	$('.lstMin').val(data.min);
	$('.appointment').removeClass('selected');
	$(this).addClass('selected');
})
.on('click','.del',function(){
	var d = JSON.parse(localStorage.apmtData);
	d.splice($(this).closest('.appointment').index(), 1);
	localStorage.apmtData = JSON.stringify(d);
	$(this).closest('.appointment').fadeOut();
})
.on('click','.btnSave',function(e){
	if($('.selected').length>0)
	{
		var i = $('.selected').index();
		var d=JSON.parse(localStorage.apmtData);
		d.splice(i, 1);
		localStorage.apmtData = JSON.stringify(d);
		$('.selected').hide();
	}
	$('.btnNew').trigger('click');
})
.on('click','.btnNew',function(e){
	var d = {hr:parseInt($('.lstHr').val()),min:parseInt($('.lstMin').val()),desc:$('.txtdesc').val()};
	var ht = '<div class="appointment"><span>'+$('.txtdesc').val()+'</span><span class="del">x</span></div>';
	$('.appointmentList').append(ht);
	$('.appointment:last').attr('data',JSON.stringify(d));
})