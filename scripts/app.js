var getLocalData = function(){
	return localStorage['apmtData']?JSON.parse(localStorage['apmtData']):[];
}
var addLocalData = function(data){
	var ld = getLocalData();
	ld.push(data)
	localStorage['apmtData'] = JSON.stringify(ld);
}
var ListHr = React.createClass({
	render:function(){
		var lstnodes=[]
		for(var i=1;i<=24;i++)
		{
			lstnodes.push(<option value={i} key={i}>{i}</option>);
		}
		return(
			<select className="lstHr">
				{lstnodes}
			</select>
		)
	}
})
var ListMin = React.createClass({
	render:function(){
		var lstnodes=[]
		for(var i=0;i<=59;i++)
		{
			lstnodes.push(<option value={i} key={i}>{i}</option>);
		}
		return(
			<select className="lstMin">
				{lstnodes}
			</select>
		)
	}
})
var ApmtMain = React.createClass({
	getInitialState:function(){ 
		var nd = new Date();
		var hr = this.props.hr, min=this.props.min, desc=this.props.desc;
		if(desc){
			return {desc:desc,hr:hr,min:min,mode:'edit'};
		}
		return {desc:'',hr:nd.getHours(),min:nd.getMinutes(),mode:'new'}	},
	lstHrChange:function(e){ this.setState({hr: e.target.value});},
	lstMinChange:function(e){this.setState({min: e.target.value});},
	txtDescChange:function(e){this.setState({desc: e.target.value});},
	handleSubmit:function(e){
		e.preventDefault();
		//if(this.state.mode!='edit')
		if(this.state.desc.trim()!='')
			addLocalData({desc:this.state.desc,hr:this.state.hr,min:this.state.min});
		// else
		// 	editLocalData({desc:this.state.desc,hr:this.state.hr,min:this.state.min});
		this.setState(this.getInitialState());
	},
	render:function(){
		var lstHrNodes=[],lstMinNodes=[]
		for(var i=1;i<=24;i++)
		{
			lstHrNodes.push(<option value={i} key={i}>{i}</option>);
		}
		for(var i=0;i<=59;i++)
		{
			lstMinNodes.push(<option value={i} key={i}>{i}</option>);
		}
	  return(
	  	
	    <div id="apmtmain" >
	    	<div className="maincontent" id="maincontent">
		    <form onSubmit={this.handleSubmit}>
		      <div><textarea placeholder="Meeting Description.." className="txtdesc" maxLength="100" onChange={this.txtDescChange} value={this.state.desc}></textarea></div>
		      <div>
		        <select className="lstHr" onChange={this.lstHrChange} value={this.state.hr} >
					{lstHrNodes}
		        </select>
		        <select className="lstMin" onChange={this.lstMinChange} value={this.state.min} >
					{lstMinNodes}
		        </select>
		      </div>
		      <input type="button" value="Save" className="btnSave" onClick={this.handleSubmit}/>
		      <input type="submit" value="New" className="btnNew" />
		      </form>
		    </div>
	    </div>
	    
	  )
	}
})

var Sidebar = React.createClass({
	render:function(){
	  var data = getLocalData();
	 return(
	  <div id="side" className="pane pane-list pane-one">
	    <header className="pane-header pane-list-header">
	      Manage Appointments
	    </header>
	    <AppointmentList data={data}/>
	  </div>
	 ) 
	}
})
var Main = React.createClass({
	render:function(){
	 return(
	  <div className="main">
	    <Sidebar />
	    <div id="rtPane" className="pane pane-chat pane-two">
	    	<ApmtMain />
	    </div>
	  </div>
	 );
	}
});
var Appointment = React.createClass({
	// getInitialState:function(){
	// 	return {hr:this.props.hr,min:this.props.min}
	// },
	handleClick:function(){
		// ReactDOM.render(
		// 	<ApmtMain hr={this.props.hr} min={this.props.min} desc={this.props.children}/>,
		// 	document.getElementById('rtPane')
		// )
		this.props.onAppointmentClick();
	},
	render:function(){
		var data = JSON.stringify({min:this.props.min,hr:this.props.hr,desc:this.props.desc});
		return(
			<div data={data} className="appointment" onClick={this.handleClick}>
			{this.props.children}
			<span className="del">x</span>
			</div>
		)
	}
})
var AppointmentList = React.createClass({
	handleApmtClick:function(){console.log(this.props)},
	render:function(){
		var i=0; var t=this;
		var appNodes = this.props.data.map(function(apmt){
			return(
				<Appointment onAppointmentClick={t.handleApmtClick} desc={apmt.desc} key={i++} min={apmt.min} hr={apmt.hr}>
					{apmt.desc}
				</Appointment>
			);
		});
		return (
	      <div className="appointmentList" >
	        {appNodes}
	      </div>
	    );
	}
});
var LoginBox = React.createClass({
	render:function(){
		return(
			<div>
				<h1>Login</h1>
				<FrmLogin />
			</div>
		)
	}
})
var AppointmentBox = React.createClass({
	render:function(){
		return(
			<div className="appointmentBox">
				<AppointmentList data={this.props.data} />
				<FrmApp />
			</div>
		)
	}
})
var FrmLogin = React.createClass({
	getInitialState: function() {
    return {name:''};
  },
  nameChange: function(e) {
    this.setState({name: e.target.value});
  },
  handleSubmit: function(e) {
    e.preventDefault();
    var name = this.state.name.trim();
    if (!name) {
      return;
    }
    else 
    	ifLoginAllowed(name);

    // TODO: send request to the server
    this.setState({name: ''});
  },
	render:function(){
		return(
			<div>
			<div id="deviceready" className="blink">
				<p className="event listening">Manage Appointments</p>
			</div>
			<form className="frmLogin" onSubmit={this.handleSubmit} >
			    <input type="text" placeholder="Login Name" value={this.state.name} onChange={this.nameChange} />
			    <input type="submit" value="Go" />
			</form>
			</div>
		)
	}
});
var FrmApp = React.createClass({
	getInitialState: function() {
    return {desc: '', date: Date.now(), time:Date.now()};
  },
  descChange: function(e) {
    this.setState({desc: e.target.value});
  },
  timeChange: function(e) {
    this.setState({time: e.target.value});
  },
  dayChange:function(e){this.setState({day: e.target.value});},
  monthChange:function(e){this.setState({month: e.target.value});},
  handleSubmit: function(e) {
    e.preventDefault();
    var desc = this.state.desc.trim();
    var date = this.state.date.trim();
    if (!date || !desc) {
      return;
    }
    // TODO: send request to the server
    this.setState({desc: '', date: ''});
  },
	render:function(){
		var date = makeDate();
		return(
			<form className="frmAppointment" onSubmit={this.handleSubmit} >
			    <input type="text" placeholder="Appointment Description" value={this.state.desc} onChange={this.descChange} />
			    <date.lstDay onChange={this.dayChange} value={this.state.day} />
			    <date.lstMonth onChange={this.monthChange} value={this.state.month} />
			    <input type="submit" value="Post" />
			</form>
		);
	}
});

var Empty =React.createClass({render:function(){return(<div></div>)}});

ReactDOM.render(
  <LoginBox />,
  document.getElementById('login')
);


var saveLoginDetails = function(name){
	name=name?name:'Rahul';
	chrome.storage.sync.set({'loginvalue': name}, function() {
          // Notify that we saved.
          //message('Settings saved');
        });
}
var getLoginDetails = function(){
	chrome.storage.sync.get('loginvalue',function(name){
		console.log(name);
		return name;
	})
}
var ifLoginAllowed = function(name){
	if(localStorage['apmtLogin']==name)
		ReactDOM.render(
			<Main />,
			document.getElementById('content')
		);
}
var addMinutes = function(date, minutes) {
    return new Date(date.getTime() + minutes*60000);
}
var makeDate = function(){
	var Opts = React.createClass({
		render:function(){
			return(<option value={this.props.key}>{this.props.children}</option>)
		}
	})
	var List = React.createClass({
		render:function(){
			var lstnodes = this.props.data.map(function(n){
				return(
					<Opts key={n}>{n}</Opts>
				)
			})
			return (
		      // <ListView  dataSource={this.state.dataSource} 
		      // renderRow={(rowData)=><Text>{rowData}</Text>} />
		      <select>{lstnodes}</select>
		    );
		}
	});
	var LstDay = React.createClass({
		render:function(){
			var a=[],i=0;
			for(i=0;i<31;i++)
				a.push(i+1);
			return(<List className="lstDay"  data={a}/>)
		}
	})
	var LstMonth = React.createClass({
		render:function(){
			var months=["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],a;
			return(
				<List className="lstMonth" data={months}/>
			)
		}
	})
	return {lstDay:LstDay,lstMonth:LstMonth}

}